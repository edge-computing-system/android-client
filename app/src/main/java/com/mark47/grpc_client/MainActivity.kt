package com.mark47.grpc_client

import com.mark47.echo.ReplyerServiceGrpcKt
import com.mark47.echo.Echo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import io.grpc.ManagedChannelBuilder
import kotlinx.coroutines.runBlocking

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val grpcBtn = findViewById<Button>(R.id.send_button)
        val tvResponse = findViewById<TextView>(R.id.grpc_response_text)
        grpcBtn.setOnClickListener {


            val host = "192.168.1.96"
            val port = 50051

            val etHost = findViewById<EditText>(R.id.host_edit_text)
            val etPort = findViewById<EditText>(R.id.port_edit_text)
            val etMessage = findViewById<EditText>(R.id.message_edit_text)

            if (etHost.text.toString() != "" && etPort.text.toString() != "") {
                runBlocking {

                    val channel =
                        ManagedChannelBuilder.forAddress(host, port).usePlaintext().build()
                    val stub = ReplyerServiceGrpcKt.ReplyerServiceCoroutineStub(channel)

                    val request = Echo.EchoReplyMsg.newBuilder().setPayload(etMessage.text.toString()).build()
                    val response = stub.reply(request)
                    tvResponse.text = response.toString()
                }
            }
        }

    }


}